public class Main {
    public static void main(String[] args) {
        NumbersUtil numbersUtil = new NumbersUtil();
        System.out.println(numbersUtil.isPrime(13)); // true
        System.out.println(numbersUtil.isPrime(31)); // true
        System.out.println(numbersUtil.isPrime(169)); // false
        System.out.println(numbersUtil.isPrime(172)); // false
        System.out.println(numbersUtil.isPrime(2)); // true
        System.out.println(numbersUtil.isPrime(3)); // true
    }
}
