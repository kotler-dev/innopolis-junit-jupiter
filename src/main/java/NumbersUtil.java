public class NumbersUtil {
    public boolean isPrime(int number) {
        if (number == 0 || number == 1) {
            throw new IllegalArgumentException();
        }
        if (number == 2 || number == 3) {
            return true;
        }
//        for (int i = 2; i < number / 2; i++) {
        for (int i = 2; i * i <= number; i++) {
            if (number % i == 0) {
                return false;
            }
        }
        return true;
    }

    public int sum(int a, int b) {
        return  a + b;
    }

    /*
    нод(18, 12) -> 6
    нод(9, 12) -> 3
    нод(64, 48) -> 16
    Предусмотреть, когда на вход подаются "некрасивые числа"
    Отрицательные числа -> исключение
    */
    public int gcd(int a, int b) {
        // Поиск наибольшего общего делителя
        return -1;
    }
}
