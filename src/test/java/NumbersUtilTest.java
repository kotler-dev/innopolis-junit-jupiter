import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName(value = "NumbersUtil is working when")
@DisplayNameGeneration(value = DisplayNameGenerator.ReplaceUnderscores.class)
class NumbersUtilTest {

    // То, что мы будем тестировать
    private final NumbersUtil numbersUtil = new NumbersUtil();

    @Nested
    @DisplayName("isPrime() is working")
    public class ForIsPrime {
        @ParameterizedTest(name = "return <true> on {0}")
        @ValueSource(ints = {2, 3, 71, 113}) // Простые числа
        public void on_prime_numbers_return_true(int number) {
            assertTrue(numbersUtil.isPrime(number));
        }

        @ParameterizedTest(name = "return <false> on {0}")
        @ValueSource(ints = {6, 63, 121, 6697}) // Непростые / составные числа
        public void on_not_prime_numbers_return_false(int number) {
            assertFalse(numbersUtil.isPrime(number));
        }

        @ParameterizedTest(name = "return <false> on {0}")
        @ValueSource(ints = {121, 169}) // Непростые, которые могут дать true при sqrt
        public void on_sqrt_numbers_return_false(int sqrtNumber) {
            assertFalse(numbersUtil.isPrime(sqrtNumber));
        }

        @ParameterizedTest(name = "throw exception on {0}")
        @ValueSource(ints = {0, 1})
        public void bad_number_throw_exception(int badNumber) {
            assertThrows(IllegalArgumentException.class, () -> numbersUtil.isPrime(badNumber));
        }
    }

    @Nested
    @DisplayName("sum() is working")
    public class ForSum {
        @ParameterizedTest(name = "{0} + {1} = {2}")
        @CsvSource(value = {"5, 10, 15", "4, 2, 6", "11, -2, 9"})
        public void return_correct_sum(int a, int b, int result) {
            assertEquals(result, numbersUtil.sum(a, b));
        }
    }

    // Написать модульный тест на int gcd()

}